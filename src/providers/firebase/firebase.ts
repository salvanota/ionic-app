import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { Device } from '@ionic-native/device';
import firebase from 'firebase/app';
import { NativeStorage } from '@ionic-native/native-storage';

@Injectable()
export class FirebaseProvider {

	public user: any;
	public minhaEntidade: AngularFireObject<any[]>;
	public entidadeDetalhes: any;
	public listaEntidades: Observable<any[]>;

	constructor(
		public afd: AngularFireDatabase, 
		private device: Device, 
		private firebaseAuth: AngularFireAuth,
		private nativeStorage: NativeStorage) { 
		
		this.firebaseAuth.auth.signInAnonymously().catch(function(error) {
			console.log(error.message);

		}).then(user => {
			this.user = user;
			//this.getMinhaEntidade();
		});
		console.log('User: '+this.user)
	}

	getEntidades() {
		console.log(this.afd.list('entidades').valueChanges());
		this.listaEntidades = this.afd.list('entidades/').snapshotChanges();
		return this.listaEntidades;
		//return this.afd.list('entidades').valueChanges();
		//return Array.of(this.afd.list('/entidades/'));
	}

	minhasNotasDoadas(){
		return this.afd.list('/notas/', (ref) => ref.orderByChild('deviceId').equalTo(this.device.uuid) );
	}

	getUser(){
		return this.user;
	}

	getMinhaEntidade(){
		this.nativeStorage.getItem('minhaEntidade')
		.then(
			data => {
				this.minhaEntidade = data;
			},
			error => console.error(error)
			);
		if(!this.minhaEntidade){
			let entidade = this.afd.object('/users/'+this.user.uid);
			entidade.valueChanges().subscribe((entidade: any) => {
				console.log(entidade);
				this.minhaEntidade = entidade.minhaEntidade;
				return entidade;
			});
		}
	}

	setMinhaEntidade(cnpj){
		this.nativeStorage.setItem('minhaEntidade', cnpj)
		.then(
			() => console.log('Stored item!'),
			error => console.error('Error storing item', error)
			);
		let profileRef = this.afd.object('users/'+this.user.uid);
		profileRef.set({
			minhaEntidade: cnpj,
			deviceid: this.device.uuid
		});
		this.minhaEntidade = cnpj;
		this.getMinhaEntidade();
	}

	getDetalhesEntidade(cnpj: string){
		let entidade = this.afd.object('entidades/'+cnpj+'/detalhes/');
		entidade.snapshotChanges().subscribe((snapshot: any) => {
			console.log('Snap: '+snapshot.payload.nome);
			this.entidadeDetalhes = snapshot.val();
		});
	}

	addNota(chNFe, vNF, vICMS) {
		let currentDate = new Date().toLocaleDateString();
		let notas = this.afd.list('/notas');
		notas.push({
			chaveNota: chNFe,
			valor: vNF,
			vICMS: vICMS,
			entidade: this.minhaEntidade,
			plataforma: this.device.platform,
			uid: this.user.uid,
			deviceId: this.device.uuid,
			date: currentDate
		}).then(foi => {
			console.log(foi);
			console.log('Foi a nota'+chNFe);
			return true;

		}, naofoi => {
			console.log(naofoi);
			return false;
		});
	}

	removeItem(id) {
		this.afd.list('/shoppingItems/').remove(id);
	}

}