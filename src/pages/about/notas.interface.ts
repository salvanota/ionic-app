export interface Notas{
	chaveNota: string;
	valor: number;
	vICMS: number;
	uid: string;
	entidade: string;
	plataforma: string;
	deviceId: string;
	date: Date;
};