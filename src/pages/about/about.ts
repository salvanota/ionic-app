import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { AngularFireObject } from 'angularfire2/database';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { HomePage } from '../home/home';

@Component({
	selector: 'page-about',
	templateUrl: 'about.html'
})
export class AboutPage {
	minhaEntidade: AngularFireObject<any>;
	options: BarcodeScannerOptions;
	results: any;
	toggleStatus: any;
	constructor(public navCtrl: NavController, 
		private firebaseProvider: FirebaseProvider, 
		private barcode: BarcodeScanner, 
		private toastCtrl: ToastController) {
		this.minhaEntidade = this.firebaseProvider.minhaEntidade;
	}

	async scanBarcode(){
		this.options = {
			preferFrontCamera: false,
			disableSuccessBeep: false
		}
		this.barcode.scan().then(resultado => {
			const url = new URL(resultado.text);
			const params = new URLSearchParams(url.search);
			var chNFe = params.get('chNFe');
			var vNF = params.get('vNF');
			var vICMS = params.get('vICMS');
			var cDest = params.get('cDest');
			if(!chNFe){
				this.presentToast('Ops, não parece uma nota valida. Tente novamente.');
			}else{
				if(!cDest){
					this.firebaseProvider.addNota(chNFe, vNF, vICMS);
					this.presentToast('Parabéns, nota enviada com sucesso!');
				}else{
					this.presentToast('Ops, CPF '+cDest+' vinculado na nota. Não pode ser doada!');
				}
			}
			if(this.toggleStatus == true){
				this.scanBarcode();
			}
		}).catch(error => {
			console.log(error);
		});
	}
	temEntidade(){
		this.firebaseProvider.getMinhaEntidade();
		if(this.firebaseProvider.minhaEntidade){
			return true;
		}else{
			return false;
		}
	}
	presentToast(message) {
		let toast = this.toastCtrl.create({
			message: message,
			position: 'middle',
			duration: 1500
		});
		toast.present();
	}
	goToEntidadesCadastradas(){
		this.navCtrl.push(HomePage);
	}
}