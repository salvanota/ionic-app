import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/toPromise';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-detalhes-entidade',
  templateUrl: 'detalhes-entidade.html',
})
export class DetalhesEntidadePage {
	entidadeCnpj: any;
	entidadeDetalhes: any;
	minhaEntidade: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private firebaseProvider: FirebaseProvider, private toastCtrl: ToastController, private socialSharing: SocialSharing) {
  	this.entidadeCnpj = this.navParams.get('entidadeCnpj');
  	this.minhaEntidade = this.firebaseProvider.minhaEntidade;
  	this.firebaseProvider.getDetalhesEntidade(this.entidadeCnpj);
  	this.entidadeDetalhes = this.firebaseProvider.entidadeDetalhes;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalhesEntidadePage');
  }

  selecionaEntidade(){
  	this.firebaseProvider.setMinhaEntidade(this.entidadeCnpj);
  	this.navCtrl.pop();
  	this.presentToast('Obrigado, agora você já pode doar.');
  }

  verificaEntidade(){
  	if(this.minhaEntidade == this.entidadeCnpj) return true; else return false;
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      position: 'middle',
      duration: 3000
    });
    toast.present();
  }

  facebookShare(){
    this.socialSharing.shareViaFacebookWithPasteMessageHint('Doe suas notas fiscais para a entidade '+this.entidadeDetalhes.nome+' você vai estar ajudando sem gastar nada :) Baixe o app: https://play.google.com/store/apps/details?id=br.com.salvanota', null, 'https://goo.gl/EnwV7R', 'Escreva uma mensagem para compartilhar...')
    .then(data => {
      console.log(data);
    }).catch(error => {
      console.log(error)
    });
  }

  twitterShare(){
      this.socialSharing.shareViaTwitter('Doe suas notas fiscais para a entidade '+this.entidadeDetalhes.nome+' você vai estar ajudando sem gastar nada :)', this.entidadeDetalhes.img, 'https://goo.gl/EnwV7R').then(result => {
        console.log(result);
      }).catch(error => {
        console.log(error);
        alert('Ops, nao foi possível compartilhar.')
      });
  }

  whatsappShare(){
    this.socialSharing.shareViaWhatsApp('Doe suas notas fiscais para a entidade '+this.entidadeDetalhes.nome+' você vai estar ajudando sem gastar nada :) Baixe o app: ', this.entidadeDetalhes.img, 'https://goo.gl/EnwV7R');
  }

}
