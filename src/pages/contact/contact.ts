import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AngularFireList } from 'angularfire2/database';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  minhasNotas: AngularFireList<any[]>;
  private qtdNotas: number;
  private totalNotas: number;

  constructor(public navCtrl: NavController, firebaseProvider: FirebaseProvider, public alertCtrl: AlertController) {
    this.minhasNotas = firebaseProvider.minhasNotasDoadas();

    //Quantidade de notas
    //See: https://github.com/angular/angularfire2/blob/5.0.0-rc.3/docs/version-5-upgrade.md
    this.minhasNotas.valueChanges().subscribe(notas => {
      this.qtdNotas = notas.length;
    });

    //Total das notas
    //See: https://github.com/angular/angularfire2/blob/5.0.0-rc.3/docs/version-5-upgrade.md
    this.minhasNotas.valueChanges().subscribe((notas: any) => {
      this.totalNotas = 0;
      notas.forEach(nota => {
        console.log(nota.vICMS);
        this.totalNotas += Number(nota.vICMS);
      })
    })
  }

  temNota(){
    if(this.qtdNotas > 0){
      return true;
    }else{
      console.log("Não tem nota");
      return false;
    }
  }

  showAlert(nota) {
  var myDate = new Date(nota.date*1000);
    let alert = this.alertCtrl.create({
      title: 'Detalhes da nota',
      subTitle: '<p>Chave da Nota: '+nota.chaveNota+'</p><p>Valor da nota: R$'+nota.valor+'</p><p>Total em ICMS: R$'+nota.vICMS+'</p><p>Data: '+nota.date+'</p><p>Doado para entidade: '+nota.entidade+'</p>',
      buttons: ['Gostei!']
    });
    alert.present();
  }
}