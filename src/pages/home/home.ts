import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireList, AngularFireObject } from 'angularfire2/database';
import { DetalhesEntidadePage } from '../detalhes-entidade/detalhes-entidade';
import { Observable } from 'rxjs/Observable';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	listaEntidades: Observable<any[]>;
	minhaEntidade: AngularFireObject<any>;
	constructor(
		public navCtrl: NavController, 
		private firebaseProvider: FirebaseProvider) {
		this.listaEntidades = firebaseProvider.getEntidades();
	}
	
	goToDetalhesDaEntidade(entidades){
		this.navCtrl.push(DetalhesEntidadePage, {entidadeCnpj: entidades});
	}

	procuraEntidade(cnpj){
		console.log(cnpj+' = '+this.firebaseProvider.minhaEntidade);
		if(cnpj == this.firebaseProvider.minhaEntidade){
			return true;
		}else{
			return false;
		}
	}

}